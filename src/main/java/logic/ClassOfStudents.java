package main.java.logic;

public class ClassOfStudents {

    public String nameOfClass;
    private int numberOfStudents;
    private int sizeOfSong;

    //o clasa are un nume, nr de studenti si un cantec (lungimea lui)
    public ClassOfStudents(String nameOfClass, int numberOfStudents, int sizeOfSong) {

        this.nameOfClass = nameOfClass;
        this.numberOfStudents = numberOfStudents;
        this.sizeOfSong = sizeOfSong;
    }

    //metoda returneaza numarul studentului din intervalul [1,numberOfStudents] inclusiv, care ramane ultimul neeliminat
    public int findLeader() {

        //numarul studentilor eliminati a.i. sa stiu sa ma opresc din eliminat (adica atunci cand removed=nrStudents - 1
        int removedStudents = 0;

        //count creste cand profesorul se uita la un elev si se opreste cand devine egal cu sizeOfSong adica cand songul
        //ajunge la final si elevul la care se uita profesorul este eliminat
        int count;

        //index repr elevul la care se uita atunci proful. folosesc modul in incrementarea lui pt a nu depasi niciodata
        // limita array-ului
        int index = 0;

        //va retine indexul sefului clasei + 1 pt ca am inceput de la 0
        int indexLeader = 0;

        //creez un array de lungime numOfStud, toate pozitiile sunt 1 la inceput. odata ce un stud e eliminat, pozitia lui
        //devine 0
        int[] arrayOfStudents = new int[this.numberOfStudents];
        for (int i = 0; i < arrayOfStudents.length; i++) {
            arrayOfStudents[i] = 1;
        }

        do {
            count = 0;                              //incepe cantecul
            while (count < this.sizeOfSong) {
                if (arrayOfStudents[index] == 1) {  //proful se uita doar la poz egala cu 1, cele cu 0 sunt stud removed
                    count++;                        //trece o sec din cantec
                    if (count == this.sizeOfSong) { //daca cantecul a ajuns la final
                        removedStudents++;          //creste nr eliminatilor
                        break;                      //break pt ca pe asta vreau sa-l elimin, apoi merg mai departe cu un elev pt urmatoarea cantare
                    }
                }
                index = (index + 1) % this.numberOfStudents; // indexul merge mai departe doar daca cantecul nu a ajuns la final count = song
            }
            arrayOfStudents[index] = 0;             //aici se ajunge doar la finalul cantecului, elevul de la indexul unde count == song devine 0
            index = (index + 1) % this.numberOfStudents;    //indexul merge mai departe si o sa cante din nou cantecul
        } while (removedStudents < (numberOfStudents - 1)); // actiunea se repeta cat timp nu s-au eliminat (nr studenti - 1)

        for (int i = 0; i < arrayOfStudents.length; i++) {  //aici ajunge doar cand a ramas un singur stud, iar acela are singura pozitie egala cu 1
            if (arrayOfStudents[i] == 1)
                indexLeader = i;
        }

        System.out.println("Leader of class " + this.nameOfClass + " is " + "student number " + (indexLeader + 1));
        return (indexLeader + 1);  //adun 1 pt ca am inceput de la 0 numararea studentilor
    }

}
