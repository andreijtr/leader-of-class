package test.java;

import main.java.logic.ClassOfStudents;
import org.junit.Assert;
import org.junit.Test;

public class ClassOfStudentsTest {

    @Test
    public void findLeaderTest() {

        ClassOfStudents englishClass = new ClassOfStudents("englishClass", 7, 10);

        int classLeader = englishClass.findLeader();
        Assert.assertEquals(5,classLeader);
    }
}
